const fs = require("fs");
const papa = require("papaparse");

function evaluateChunk(evaluatedObject, chunk) {
    let output;
    output = evaluatedObject[chunk];
    if (output.meta?.type == "matrix") {
        let matrixAttributes = Object.keys( output.value[0] );
        matrixAttributes.forEach( attribute => {
            output[attribute] = { value: evaluatedObject[chunk].value.map( d => d[attribute].value ) };
        } );
    };
    return output;
};

function digSurvey(object, path, defaultValue = null) {
    const chunks = path
          .split(/\.|\[|\]/)
          .filter(d => d.length !== 0)
    ;
    try {
        const value = chunks.reduce((current, chunk) => evaluateChunk(current, chunk), object);
        if (value !== undefined) {
            return value;
        } else {
            return defaultValue;
        }
    } catch (e) {
        return defaultValue;
    }
};

let example = JSON.parse(fs.readFileSync(`${__dirname}/../exampleData/full_submission.json`));
let backgroundScoring = papa.parse( fs.readFileSync(`${__dirname}/../scoring_tables/background_and_community.csv`, "utf8"), {dynamicTyping:true, header:true} )
    .data
    .filter( d => d.path )
;

// testing matrix digger
// matrix
evaluateChunk( example[0].data.identity, "women_owned" );
digSurvey(example[0], 'data.identity.women_owned.women_owned.value');
// regular
digSurvey( example[0], 'data.outcomes.details.value' );

let usedPaths = new Set( backgroundScoring.map(d  => d.path ) );


let womenOwned = example[0].data.identity.women_owned.value[0].women_owned.value;
let vetOwned = example[0].data.identity.veteran_owned.value[0].veteran_owned.value;
let ethnicity = example[0].data.identity.ethnicity.value;
let currentPlans = example[0].data.common_onboarding.common.management_plans.current.is_true.value;
let currentPlansValue = example[0].data.common_onboarding.common.management_plans.current.detail.value;
let networks = example[0].data.engagement.hubs_networks.value;
let outcomes = example[0].data.outcomes.details.value;

let obtainedAnswers = backgroundScoring.map( answer => {
    let output = {
        path: answer.path,
        answer: answer.answer_value,
        answer_label: answer.answer,
        answer_found: digSurvey(example[0], answer.path.concat(".value")),
        max_total_section: answer.max_total_section,
        max_total_subsection: answer.max_total_subsection,
        section: answer.section,
        subsection: answer.subsection,
        points: answer.points_adjusted
    };
    output.mentioned = Array.isArray(output.answer_found) ? output.answer_found.includes(output.answer ) : output.answer_found == output.answer;
    return output;
} )
;

obtainedAnswers.filter( d => d.mentioned );

let scoringSections = {};

obtainedAnswers
    .filter( d => d.mentioned )
    .forEach( answer => {
        if (!scoringSections[answer.section]) {
            scoringSections[answer.section] = {};
        };
        if (!scoringSections[answer.section][answer.subsection]) {
            scoringSections[answer.section][answer.subsection] = {
                answers: [],
                subsection_score: 0
            };
        };
        scoringSections[answer.section][answer.subsection].answers.push(answer);
        scoringSections[answer.section][answer.subsection].max_subsection = answer.max_total_subsection;
        scoringSections[answer.section].max_section = answer.max_total_section;
        scoringSections[answer.section][answer.subsection].subsection_score = scoringSections[answer.section][answer.subsection].subsection_score + answer.points;
    } )
;

scoringSections['BACKGROUND/COMMUNITY ENGAGEMENT'].PROGRAMS.answers;
