const fs = require("fs");

let data = JSON.parse( fs.readFileSync( "../exampleData/survey_submissions.json" ) );

let firstLevelKeys = Object.keys( data[0].data );

class ScoreAssigner {
    constructor({
    }) {
    };
};


let background_community_engagement = [
    {
        question: "is the ranch women owned?",
        data_source: data[0].data.identity.women_owned.value[0].women_owned.value,
        values: [ "yes", "no" ]
    },
    {
        question: "is the ranch veteran owned?",
        data_source: data[0].data.identity.veteran_owned.value[0].veteran_owned.value,
        values: [ "yes", "no" ]
    },
    {
        question: "ehtnicity",
        data_source: data[0].data.identity.ethnicity.value[0].ethnicity.value,
        // get ontology
        // value: [ 'asian' ]
    }
];
